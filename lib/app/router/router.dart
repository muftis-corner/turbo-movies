import 'package:flutter/material.dart';
import 'package:turbo_movies/app/screen/getting-started/gettingStartScreen.dart';
import 'package:turbo_movies/app/screen/home/homeScreen.dart';
import 'package:turbo_movies/app/screen/login/loginScreen.dart';
import 'package:turbo_movies/app/screen/splash/splashScreen.dart';

class AppRouter {
  static const String splash = '/splash';
  static const String gettingStart = '/getting-start';
  static const String login = '/login';
  static const String home = '/home';

  static Map<String, Widget Function(BuildContext)> routes = {
    splash: (BuildContext context) => const SplashScreen(),
    gettingStart: (BuildContext context) => const GettingStartScreen(),
    login: (BuildContext context) => const LoginScreen(),
    home: (BuildContext context) => const HomeScreen(),
  };
}
