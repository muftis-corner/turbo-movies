import 'dart:async';

import 'package:flutter/material.dart';
import 'package:turbo_movies/app/router/router.dart';
import 'package:turbo_movies/app/widgets/logo/logo.dart';
import 'package:turbo_movies/data/const/hero.dart';
import 'package:turbo_movies/device/utils/ScreenProp.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  void timer(BuildContext context) {
    Timer(Duration(seconds: 1), () {
      Navigator.popAndPushNamed(context, AppRouter.login);
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenProp.init(MediaQuery.of(context));
    timer(context);
    return Scaffold(
      body: Center(
        child: Hero(
          tag: HeroTag.logoHero,
          child: AppLogo(
            size: 125,
          ),
        ),
      ),
    );
  }
}
