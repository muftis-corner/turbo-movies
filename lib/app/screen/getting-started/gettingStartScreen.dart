import 'package:flutter/material.dart';
import 'package:turbo_movies/app/screen/login/widget/loginForm.dart';
import 'package:turbo_movies/app/widgets/logo/logo.dart';
import 'package:turbo_movies/data/const/hero.dart';
import 'package:turbo_movies/device/utils/ScreenProp.dart';

class GettingStartScreen extends StatelessWidget {
  const GettingStartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: ScreenProp.height / 8),
              child: Hero(
                tag: HeroTag.logoHero,
                child: AppLogo(
                  size: 50,
                ),
              ),
            ),
            LoginForm()
          ],
        ),
      ),
    );
  }
}
