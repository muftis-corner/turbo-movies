import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:turbo_movies/app/bloc/movie/movieBloc.dart';
import 'package:turbo_movies/app/bloc/movie/movieEvent.dart';
import 'package:turbo_movies/app/screen/home/widgets/movieItem.dart';
import 'package:turbo_movies/app/screen/home/widgets/movieList.dart';
import 'package:turbo_movies/data/const/color.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.appBar,
        brightness: Brightness.light,
        centerTitle: true,
        title: Text(
          'Movie Turbo',
          style: TextStyle(color: AppColor.primary),
        ),
      ),
      body: BlocProvider(
        create: (context) => MovieBloc()..add(MovieFetched()),
        child: MovieList(),
      ),
    );
  }
}
