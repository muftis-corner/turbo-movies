import 'package:flutter/material.dart';
import 'package:turbo_movies/data/const/color.dart';
import 'package:turbo_movies/data/model/movie.dart';
import 'package:turbo_movies/data/utils/stringCutter.dart';

class MovieItem extends StatelessWidget {
  final Movie movie;
  const MovieItem({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      height: 150,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Colors.grey, width: 1)),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            flex: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  bottomLeft: Radius.circular(15)),
              child: Image.network(
                '${movie.cover}',
              ),
            ),
          ),
          Flexible(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(top: 16, left: 16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${StringCutter.cutoff(movie.title, 30)}',
                      textAlign: TextAlign.start,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: List.generate(
                          movie.rating ~/ 2,
                          (index) => Text(
                            '⭐️ ',
                            style: TextStyle(fontSize: 20),
                          ),
                        )),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: List.generate(movie.genres.length, (index) {
                          if (index + 1 == movie.genres.length) {
                            return Text(
                              movie.genres[index],
                              style: TextStyle(color: AppColor.primary),
                            );
                          } else {
                            return Row(
                              children: [
                                Text(
                                  movie.genres[index],
                                  style: TextStyle(color: AppColor.primary),
                                ),
                                TagDivider()
                              ],
                            );
                          }
                        }),
                      ),
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}

class TagDivider extends StatelessWidget {
  const TagDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: 20,
      width: 1,
      color: AppColor.primary,
    );
  }
}
