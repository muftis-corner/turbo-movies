import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:turbo_movies/app/bloc/movie/movieBloc.dart';
import 'package:turbo_movies/app/bloc/movie/movieEvent.dart';
import 'package:turbo_movies/app/bloc/movie/movieState.dart';
import 'package:turbo_movies/app/screen/home/widgets/movieItem.dart';
import 'package:turbo_movies/app/widgets/loader/circleLoader.dart';

class MovieList extends StatefulWidget {
  const MovieList({Key? key}) : super(key: key);

  @override
  _MovieListState createState() => _MovieListState();
}

class _MovieListState extends State<MovieList> {
  final _scrollController = ScrollController();
  late MovieBloc _movieBloc;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _movieBloc = context.read<MovieBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(builder: (context, state) {
      switch (state.status) {
        case MovieStatus.failure:
          return const Center(child: Text('failed to fetch posts'));
        case MovieStatus.success:
          if (state.movies.isEmpty) {
            return const Center(child: Text('no Movies available'));
          }
          return ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return index >= state.movies.length
                  ? CircleLoader()
                  : MovieItem(movie: state.movies[index]);
            },
            itemCount: state.hasReachedMax
                ? state.movies.length
                : state.movies.length + 1,
            controller: _scrollController,
          );
        default:
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) _movieBloc.add(MovieFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
