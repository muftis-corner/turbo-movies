import 'package:flutter/material.dart';

import 'package:turbo_movies/app/widgets/button/RoundedTextButton.dart';
import 'package:turbo_movies/device/utils/ScreenProp.dart';

class GettingStartedComponent extends StatelessWidget {
  final VoidCallback gettingStartOnPressed;
  const GettingStartedComponent({
    Key? key,
    required this.gettingStartOnPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'WELCOME',
          style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: ScreenProp.height / 4,
        ),
        RoundedTextButton(
            label: 'GETTING START', onPressed: gettingStartOnPressed)
      ],
    );
  }
}
