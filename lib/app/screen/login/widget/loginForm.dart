import 'package:flutter/material.dart';
import 'package:turbo_movies/app/router/router.dart';
import 'package:turbo_movies/app/widgets/button/RoundedTextButton.dart';
import 'package:turbo_movies/app/widgets/form/RoundedTextField.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  final TextStyle _hintStyle =
      const TextStyle(color: Colors.black54, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8),
          child: RoundedTextField(
            hint: 'User name',
            hintStyle: _hintStyle,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8),
          child: RoundedTextField(
            hint: 'Password',
            obscureText: true,
            hintStyle: _hintStyle,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        RoundedTextButton(
          onPressed: () {
            Navigator.popAndPushNamed(context, AppRouter.home);
          },
          label: 'LOGIN',
        ),
      ],
    );
  }
}
