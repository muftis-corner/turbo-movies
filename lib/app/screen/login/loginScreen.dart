import 'package:flutter/material.dart';
import 'package:turbo_movies/app/screen/login/widget/gettingStarted.dart';
import 'package:turbo_movies/app/screen/login/widget/loginForm.dart';
import 'package:turbo_movies/app/widgets/logo/logo.dart';
import 'package:turbo_movies/device/utils/ScreenProp.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageController controller = PageController();

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(bottom: 25),
              height: ScreenProp.height / 4,
              child: AppLogo(
                size: 80,
              ),
            ),
            Container(
              height: ScreenProp.height / 4 * 3,
              child: PageView(
                allowImplicitScrolling: false,
                controller: controller,
                children: [
                  GettingStartedComponent(gettingStartOnPressed: () {
                    controller.nextPage(
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeOut);
                  }),
                  LoginForm()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
