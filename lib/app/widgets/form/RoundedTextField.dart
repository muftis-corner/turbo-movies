import 'package:flutter/material.dart';

class RoundedTextField extends StatefulWidget {
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSubmitted;
  final VoidCallback? onEditingComplete;
  final bool obscureText;
  final String initialValue;
  final String? hint;
  final TextStyle? hintStyle;
  const RoundedTextField({
    Key? key,
    this.onChanged,
    this.onSubmitted,
    this.onEditingComplete,
    this.initialValue = '',
    this.obscureText = false,
    this.hint,
    this.hintStyle,
  }) : super(key: key);

  @override
  _RoundedTextFieldState createState() => _RoundedTextFieldState();
}

class _RoundedTextFieldState extends State<RoundedTextField> {
  TextEditingController textController = new TextEditingController();

  @override
  void initState() {
    textController.text = widget.initialValue;
    super.initState();
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      key: widget.key,
      onChanged: widget.onChanged,
      onEditingComplete: widget.onEditingComplete,
      onSubmitted: widget.onSubmitted,
      obscureText: widget.obscureText,
      decoration: InputDecoration(
        hintText: widget.hint,
        hintStyle: widget.hintStyle,
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(50),
        ),
      ),
    );
  }
}
