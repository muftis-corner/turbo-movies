import 'dart:ui';

import 'package:flutter/widgets.dart';

class LogoPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    TextStyle textStyle = TextStyle(fontSize: 30);
    TextSpan turbo = TextSpan(text: 'Turbo', style: textStyle);
    TextSpan movie = TextSpan(text: 'Movie', style: textStyle);
    TextPainter turboPainter = TextPainter(text: turbo);
    TextPainter moviePainter = TextPainter(text: movie);
    turboPainter.paint(canvas, Offset(0, 0));
    moviePainter.paint(canvas, Offset(50, 50));
  }

  @override
  bool shouldRepaint(LogoPainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(LogoPainter oldDelegate) => false;
}
