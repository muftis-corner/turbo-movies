import 'package:flutter/material.dart';
import 'package:turbo_movies/app/widgets/logo/customPainter.dart';

class AppLogo extends StatelessWidget {
  final double size;
  const AppLogo({
    Key? key,
    this.size = 100,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlutterLogo(
      size: size,
    );
    // return CustomPaint(
    //   painter: LogoPainter(),
    //   child: Container(),
    // );
  }
}
