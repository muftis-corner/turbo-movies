import 'package:equatable/equatable.dart';
import 'package:turbo_movies/data/model/movie.dart';

enum MovieStatus { intital, success, failure }

class MovieState extends Equatable {
  final MovieStatus status;
  final List<Movie> movies;
  final bool hasReachedMax;

  const MovieState(
      {this.status = MovieStatus.intital,
      this.movies = const <Movie>[],
      this.hasReachedMax = false});

  MovieState copyWith({
    MovieStatus? status,
    List<Movie>? movies,
    bool? hasRachedMax,
  }) {
    return MovieState(
        status: status ?? this.status,
        movies: movies ?? this.movies,
        hasReachedMax: hasReachedMax);
  }

  @override
  String toString() {
    return '''MovieState(status:$status, movies:${movies.length}, hasReachedMax:$hasReachedMax)''';
  }

  @override
  List<Object?> get props => [status, movies, hasReachedMax];
}
