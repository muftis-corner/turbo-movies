import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:turbo_movies/app/bloc/movie/movieEvent.dart';
import 'package:turbo_movies/app/bloc/movie/movieState.dart';
import 'package:turbo_movies/data/model/movie.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

const int limit = 20;

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc() : super(const MovieState());

  // final http.Client httpClient = http.Client();
  final Dio httpClient = Dio();

  @override
  Stream<Transition<MovieEvent, MovieState>> transformEvents(
      Stream<MovieEvent> events,
      TransitionFunction<MovieEvent, MovieState> transitionFn) {
    return super.transformEvents(
      events.throttleTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<MovieState> mapEventToState(MovieEvent event) async* {
    if (event is MovieFetched) {
      yield await _mapMovieFetchedToState(state);
    }
  }

  Future<MovieState> _mapMovieFetchedToState(MovieState state) async {
    if (state.hasReachedMax) return state;
    try {
      if (state.status == MovieStatus.intital) {
        final movies = await _fetchMovies(1);
        return state.copyWith(
          status: MovieStatus.success,
          movies: movies,
          hasRachedMax: true,
        );
      }
      final movies = await _fetchMovies(1);
      return movies.isEmpty
          ? state.copyWith(hasRachedMax: true)
          : state.copyWith(
              status: MovieStatus.success,
              movies: List.of(state.movies)..addAll(movies),
              hasRachedMax: false);
    } on Exception {
      return state.copyWith(status: MovieStatus.failure);
    }
  }

  Future<List<Movie>> _fetchMovies([int page = 0]) async {
    final response = await httpClient.get(
      'https://yts.mx/api/v2/list_movies.json',
      queryParameters: {"page": "$page"},
    );
    if (response.statusCode == 200) {
      final data = response.data['data']['movies'];
      return data.map<Movie>((json) {
        return Movie.fromMap(json);
      }).toList() as List<Movie>;
    }
    throw Exception('error Fetching movies');
  }
}
