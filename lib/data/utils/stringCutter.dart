class StringCutter {
  static String cutoff(String value, int length) {
    if (value.length > length) {
      String res = value.substring(0, length);
      return res + '...';
    }
    return value;
  }
}
