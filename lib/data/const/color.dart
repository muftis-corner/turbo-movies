import 'package:flutter/material.dart';

class AppColor {
  static const Color appBar = const Color.fromRGBO(241, 241, 241, 1);
  static const Color primary = const Color.fromRGBO(83, 136, 221, 1);
}
