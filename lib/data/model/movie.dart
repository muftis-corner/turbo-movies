import 'dart:convert';

import 'package:equatable/equatable.dart';

class Movie extends Equatable {
  final String title;
  final num rating;
  final String cover;
  final List<String> genres;

  Movie({
    required this.title,
    required this.rating,
    required this.cover,
    required this.genres,
  });

  @override
  List<Object> get props => [title, rating, cover, genres];

  Movie copyWith({
    String? title,
    double? rating,
    String? cover,
    List<String>? genres,
  }) {
    return Movie(
      title: title ?? this.title,
      rating: rating ?? this.rating,
      cover: cover ?? this.cover,
      genres: genres ?? this.genres,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'rating': rating,
      'cover': cover,
      'genres': genres,
    };
  }

  factory Movie.fromMap(Map<String, dynamic> map) {
    return Movie(
      title: map['title'],
      rating: map['rating'],
      cover: map['medium_cover_image'],
      genres: List<String>.from(map['genres'] ?? []),
    );
  }

  String toJson() => json.encode(toMap());

  factory Movie.fromJson(String source) => Movie.fromMap(json.decode(source));

  @override
  bool get stringify => true;
}
