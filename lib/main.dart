import 'dart:io';

import 'package:flutter/material.dart';
import 'package:turbo_movies/data/const/color.dart';

import 'app/router/router.dart';
import 'device/utils/httpOverrides.dart';

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Turbo Movie',
      theme: ThemeData(
        primaryColor: AppColor.primary,
        fontFamily: 'Poppins',
        primarySwatch: Colors.blue,
      ),
      routes: AppRouter.routes,
      initialRoute: AppRouter.splash,
    );
  }
}
