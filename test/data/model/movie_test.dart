import 'dart:convert';

import 'package:test/test.dart';
import 'package:turbo_movies/data/model/movie.dart';

void main() {
  group('Movie Model Test!', () {
    test('Create Movie from JSON', () {
      String fixed = sample.replaceAll('\"', "'");
      fixed = sample.replaceAll("'", '"');

      Movie result = Movie.fromJson(fixed);
      print(result);
      // expect(
      //     result,
      //     Movie(
      //         title: "December Boys",
      //         rating: 6.5,
      //         cover:
      //             "https://yts.mx/assets/images/movies/december_boys_2007/medium-cover.jpg",
      //         genres: [
      //           "Drama",
      //           "Romance",
      //         ]));
    });
  });
}

String sample = """{
    "id": 34869,
    "url": "https://yts.mx/movies/december-boys-2007",
    "imdb_code": "tt0465436",
    "title": "December Boys",
    "title_english": "December Boys",
    "title_long": "December Boys (2007)",
    "slug": "december-boys-2007",
    "year": 2007,
    "rating": 6.5,
    "runtime": 105,
    "genres": [
      "Drama",
      "Romance"
    ],
    "yt_trailer_code": "OZ4-soyzzNw",
    "language": "en",
    "mpa_rating": "PG-13",
    "background_image": "https://yts.mx/assets/images/movies/december_boys_2007/background.jpg",
    "background_image_original": "https://yts.mx/assets/images/movies/december_boys_2007/background.jpg",
    "small_cover_image": "https://yts.mx/assets/images/movies/december_boys_2007/small-cover.jpg",
    "medium_cover_image": "https://yts.mx/assets/images/movies/december_boys_2007/medium-cover.jpg",
    "large_cover_image": "https://yts.mx/assets/images/movies/december_boys_2007/large-cover.jpg",
    "state": "ok",
    "torrents": [
      {
        "url": "https://yts.mx/torrent/download/7E7E9FE03FA4321A3D6D2A58A39ADA514E074E8D",
        "hash": "7E7E9FE03FA4321A3D6D2A58A39ADA514E074E8D",
        "quality": "720p",
        "type": "web",
        "seeds": 0,
        "peers": 0,
        "size": "962.92 MB",
        "size_bytes": 1009694802,
        "date_uploaded": "2021-08-19 01:31:18",
        "date_uploaded_unix": 1629329478
      },
      {
        "url": "https://yts.mx/torrent/download/692B8585B7B2F5E0A4EAAAD8545A09D62E6F2937",
        "hash": "692B8585B7B2F5E0A4EAAAD8545A09D62E6F2937",
        "quality": "1080p",
        "type": "web",
        "seeds": 0,
        "peers": 0,
        "size": "1.93 GB",
        "size_bytes": 2072321720,
        "date_uploaded": "2021-08-19 03:47:07",
        "date_uploaded_unix": 1629337627
      }
    ],
    "date_uploaded": "2021-08-19 01:31:18",
    "date_uploaded_unix": 1629329478
  }""";
